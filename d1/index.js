// Section Comparison Query Operators
// $gt/$gte - allows us to find the documents that have field number values greater than or equal to a specified number
/*
	syntax:
	db.collectionName.find({field: {$gt:value}});
	gt - greater than
	gte - greater than or equal to
*/

db.users.find({age: {$gt: 50}});
db.users.find({age: {$gte: 50}});

/*
	lt - less than
	lte - less than or equal to
*/

db.users.find({age: {$lt: 50}});
db.users.find({age: {$lte: 50}});

// $ne operator
/*
	syntax:
		db.collectionName.find({field: {$ne: value}});
*/

db.users.find({age: {$ne: 82}});

// $in operator
/*
	allows us to find specific match criteria on one field using different values
	syntax:
	db.collectionName.find({field: {$in: value}});
*/

db.users.find({lastName: {$in: ["Hawking","Doe"]}});

// section - logical query operator

db.users.find({$or:[{firstName: "Neil"}, {age: {$gt: 21}]});

// and operator

db.users.find({$and: [{age:{$ne:82}}, {age: {$ne:76}}]});

// Section - Field projection

/*
	- retrieving documents are common operations that we do and by default MongoDB queries to return the whole document as a response
	- when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish

	syntax:
		db.collectionName.find({criteria}, {field:1});
*/

// Inlcusion
db.users.find(
	{firstName: "Jane"},
	{
		firstName:1, // values can be true or false to modify the search
		lastName:1,
		contact:1
	}
);

//mini activity

db.users.find(
	{firstName: "Jane"},
	{
		id:0
	}
);

//returning specific fields in embedded documents

db.users.find(
	{firstName: "Jane"},
	{
        lastName: 1,
        contact: {phone: true}
	}
);

//
db.users.find({
	{firstName: "Jane"},
	{
		firstName:1,
		lastName:1,
		"contact.phone": 1 // can be change to or false to supress specific data
	}

});

// project specific array elements in the returned array

// $slice operator

db.users.find(
	{
		"nameArr":{nameA: "Juan"}
	},
          {
              nameArr:{$slice: 1}
          }
);

//Section - evaluation query operators
//$regex operator
/*
	allow us to find documents that match a specific string pattern using regular expressions
	syntax:
	db.collectionName.find({field: $regex: "pattern", $options: "$optionsValue"});
*/

db.users.find({firstName: {$regex: "N"}});
// case-insensitive query
db.users.find({firstName: {$regex: "j", $options: "$i"}});

//using or, this can also be done through the code below
/*
	db.users.find({$or: [{firstName: {$regex: "n"}},{firstName: {$regex: "N"}}]});
*/
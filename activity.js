// users with s and t on their last name, showing only firstname and last name

db.users.find(
	{$or: [{
		firstName: {$regex: "s"}
	},
	{
		lastName: {$regex: "t"}
	}
	]},
	{
		firstName:1,
		lastName: 1,
		_id:0
	}
);

// users from HR department & age >= 70

db.users.find(
	{$and: [{department:"HR"}, 
	{age: {$gte:70}}]}
);

//users with letter "e" on their firstName and age <= 30

db.users.find(
	{$and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}]}
);